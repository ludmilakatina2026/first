# Алгоритмы балансировки нагрузки
Существует несколько алгоритмов балансировки нагрузки, которые могут быть использованы в различных ситуациях. 

1. **Round robin (круговой выбор)**: В этом методе обращения к серверам нагрузки, запросы распределяются равномерно между серверами, поочередно. То есть каждый новый запрос отправляется на следующий сервер в очереди, и после достижения последнего сервера, следующий запрос отправляется на первый сервер.


*Особенности*:

-Равное распределение времени: Round robin обеспечивает равное распределение процессорного времени между всеми задачами, что позволяет избежать "голодания" процессов с меньшим приоритетом.

-Простота реализации: алгоритм Round robin относительно прост в реализации и может быть использован на различных операционных системах.

-Поддержка многопроцессорных систем: алгоритм Round robin также может быть использован для распределения задач между несколькими процессорами.

*Недостатки*:

-Увеличение задержек: круговая обработка задач может привести к увеличению задержек, так как процессу может потребоваться дополнительное время для ожидания, пока процессор освободится.

-Неэффективность для длительных задач: если в системе присутствуют задачи, которые требуют значительного процессорного времени, то алгоритм Round robin может быть неэффективен.

-Проблемы при выборе временного интервала: выбор оптимального временного интервала может оказаться сложной задачей, так как он должен быть достаточно коротким, чтобы обеспечить равное распределение времени между задачами, но не слишком маленьким, чтобы не увеличить задержки и не снизить производительность.

2. **Least connections (минимальное количество соединений)**:  Данный алгоритм выбирает сервер с текущим наименьшим количеством соединений и отправляет запрос на него. Это позволяет распределить нагрузку на серверы более равномерно, так как серверы с наименьшей нагрузкой будут получать больше запросов.

*Основное преимущество* этого алгоритма заключается в том, что он обеспечивает равномерное распределение нагрузки, так как он назначает новые соединения серверу с наименьшей нагрузкой. Это помогает избежать перегрузки сервера, которая может привести к потере доступности для клиентов. 

Однако у алгоритма LC есть и *недостатки*. Он не обязательно учитывает скорость обработки запросов сервером, что может привести к тому, что медленный сервер получит большую нагрузку в сравнении с более быстрыми серверами. Это может снизить скорость обработки запросов для клиентов. Кроме того, если у сервера очень мало или совсем нет соединений, а другие сервера имеют много соединений, то LC может продолжать назначать новые соединения на малозагруженный сервер, даже если это не самый эффективный выбор. 

В целом, алгоритм LC хорошо подходит для крупных сетевых приложений с большим количеством соединений, где требуется равномерное распределение нагрузки. Однако он не идеален для всех сценариев и может потребоваться более сложный алгоритм балансировки нагрузки для определенных ситуаций.

3. **IP hash (хэш IP-адреса)**: В этом методе распределения запросов, сервер выбирается на основе хэша IP-адреса клиента. Сервер выбирается из заранее определенного набора серверов на основе хэш-функции, а затем запрос клиента отправляется на выбранный сервер. Этот метод является полезным в многопроцессорных системах, так как он позволяет установить соединение с тем же сервером каждый раз, когда клиент отправляет запрос.

Основные *преимущества* использования IP hash:

-Простота в настройке и наиболее распространен в сетях виртуальных серверов.  
-Он автоматически обеспечивает сохранение состояния соединения, чтобы запросы от одного IP-адреса всегда направлялись на один и тот же сервер.  
-IP hash хорошо подходит для сценариев, где клиенты подключаются к одному серверу, а затем последуют застройщика к другому серверу в кластере.  
-Он не требует дополнительного оборудования.

*Недостатки IP hash:*

-При использовании IP hash, нагрузка не всегда равномерно распределяется между серверами в кластере, так как в теории запросы от одного и того же клиента могут быть отправлены на один и тот же сервер.   
-IP hash может быть уязвимым для DDoS-атак, так как клиенты с одним и тем же IP-адресом будут направлять запросы на один и тот же сервер, что может перегрузить его.   
-Несмотря на то, что IP hash автоматически сохраняет состояние соединения, это также означает, что если сервер отказывает, клиент не перенаправится на другой сервер, что может привести к сбоям или прерываниям в обслуживании клиентов.  
-Использование IP hash подчинено ограничениям IPv4-адресов и может не работать должным образом в сетях, где используется NAT.

4. **Random (случайный выбор)**: Как следует из названия, этот алгоритм случайным образом выбирает сервер в пуле серверов. Этот метод используется тогда, когда нет строгих требований к распределению запросов между серверами.
Он прост в реализации и не требует большого количества вычислительных ресурсов для его работы. 

Однако у данного алгоритма есть *недостатки*. Так, если на сервере нагрузка будет высокой, то вероятность выбора этого сервера в следующем запросе повышается, что может привести к еще большей нагрузке на него и снижению производительности системы в целом. Кроме того, этот алгоритм не учитывает все параметры серверов и может не равномерно распределять нагрузку между ними, что может привести к неэффективному использованию ресурсов. 

Из-за данных недостатков, алгоритм балансировки нагрузки Random используется достаточно редко в производственных системах и предпочтение отдается, например, алгоритмам Round Robin или Least Connections.

5. **Weighted Round Robin** - это метод балансировки нагрузки, который распределяет трафик между серверами или устройствами с учетом веса, который присваивается каждому серверу или устройству. Это не идеальный подход, но он значительно лучше обычного Round Robin.  

Основные *преимущества* Weighted Round Robin:

-Распределение трафика с учетом потенциальной производительности каждого сервера или устройства.

-Можно присвоить больший вес серверам или устройствам с более мощными характеристиками.

-Легко настроить и использовать.

-Поддерживает масштабирование, поскольку новые серверы или устройства могут быть добавлены и взвешены с учетом производительности.

*Недостатки* Weighted Round Robin:

-Неудобство в настройке: для правильной настройки метода необходимо учитывать характеристики каждого сервера или устройства.  
-Проблемы с масштабированием при значительном увеличении числа серверов: с увеличением количества серверов или устройств может возникнуть сложность в настройке всех весов.

-Невозможность учитывать фактическую нагрузку на сервера: веса распределяются исходя из ожидаемой загрузки, а не из реальной.

-Отсутствие управления состоянием: метод не учитывает наличие клиентов или их сессии на сервере.  

6. **Sticky sessions**, также известные как session affinity, - это технология балансировки нагрузки, которая позволяет сессии пользователя оставаться на одном сервере в течение всего времени сеанса. Это означает, что если пользователь отправляет несколько запросов на сервер, все они будут обрабатываться на том же сервере, что и первоначальный запрос. 

Одним из главных *преимуществ* sticky sessions является уменьшение нагрузки на сеть и серверы, так как все запросы пользователя уходят на один и тот же сервер. Кроме того, это улучшает производительность, так как некоторые приложения могут терять состояния, если запросы клиента обрабатываются на разных серверах. 

Однако, существуют и *недостатки*. Первым и главным компромиссом sticky sessions является отсутствие балансировки нагрузки. Поскольку все запросы отправляются на один сервер, другие серверы могут оставаться не использованными. Это также может привести к перегрузке сервера, особенно если у вас есть несколько сеансов на одном сервере. 

Кроме того, sticky sessions также могут привести к проблемам масштабирования, так как при увеличении количества серверов необходимо поддерживать контроль работоспособности и отказоустойчивости между серверами.

В целом, выбор использования sticky sessions должен основываться на конкретных потребностях вашего приложения. Если необходимо сохранять сессии пользователей, то sticky sessions могут быть хорошим вариантом, но если нагрузка на серверы большая, другие методы балансировки нагрузки могут быть более эффективными.



7. **Динамическая балансировка нагрузки**- это процесс перераспределения нагрузки между серверами в режиме реального времени на основе текущих условий сети, общей загрузки и доступности ресурсов. Она используется для повышения производительности, уменьшения времени простоя и улучшения общего опыта пользователя.

*Особенности* динамической балансировки нагрузки:

-Обеспечение высокой доступности - если какой-то сервер перегружен, средства динамической балансировки нагрузки могут перенаправить трафик на другой сервер.

-Равномерная загрузка - динамическая балансировка нагрузки может равномерно распределить трафик между несколькими серверами и уменьшить вероятность перегрузки одного из серверов.

-Улучшенная производительность - распределение нагрузки по нескольким серверам снижает риск перегрузки и повышает производительность.

-Лучшая масштабируемость - динамическая балансировка нагрузки позволяет легко добавлять новые серверы в сеть, что облегчает масштабирование сети.

*Недостатки* динамической балансировки нагрузки:

-Дополнительные затраты на оборудование и программное обеспечение - динамическая балансировка нагрузки требует дополнительного оборудования и программного обеспечения для реализации.

-Сложность настройки и управления - настройка и управление динамической балансировкой может быть сложным и требовать опыта в области сетевых технологий.

-Снижение производительности при выходе из строя - если сервер, отвечающий за динамическую балансировку нагрузки, выходит из строя, производительность всей сети может снизиться, что может повлиять на пользователей.


8. **Shortest Job First - SJF (Метод наименьшего времени отклика )** - это алгоритм планирования процессов в операционных системах, который стремится минимизировать среднее время ожидания процессов в очереди. 

*Особенности* метода:

-Процессы с наименьшим временем выполнения будут обслужены первыми, что помогает уменьшить время ожидания для всех процессов в очереди.

-Метод SJF может использоваться как на уровне ядра операционной системы, так и на уровне пользователя.

-Когда используется метод SJF на уровне ядра операционной системы, процессы, которые меньше всего ждут, будут иметь более высокий приоритет и выполняться раньше остальных.

*Недостатки метода*:

-Метод SJF может привести к длительным задержкам для процессов с длинным временем выполнения, поскольку они будут ждать выполнения более коротких процессов.

-Для применения метода SJF требуется знать время выполнения каждого процесса заранее, что в реальных системах часто невозможно.

-Метод SJF не учитывает приоритеты процессов и может привести к тому, что некоторые процессы будут несправедливо длительное время ждать выполнения.


9. **Метод на основе ресурсов**
В методе на основе ресурсов балансировщики нагрузки распределяют трафик, анализируя текущую нагрузку на сервер. Специализированное программное обеспечение, называемое агентом, работает на каждом сервере и рассчитывает использование ресурсов сервера, таких как вычислительная мощность и память. Затем балансировщик нагрузки проверяет агент на наличие достаточного количества свободных ресурсов перед распределением трафика на этот сервер.  
Такой алгоритм позволяет снизить нагрузку на отдельный сервер, распределяя ее на другие серверы, что помогает поддерживать производительность системы и обеспечивает отказоустойчивость.

Одним из наиболее распространенных *недостатков* такого алгоритма является сложность его настройки и обслуживания. Для использования алгоритма требуется постоянное мониторинг ресурсных параметров всех серверов, что может потребовать значительных усилий и ресурсов. Кроме того, необходимо понимание того, какие ресурсы являются наиболее важными для конкретного приложения или сервиса, так как именно они должны получать наибольшее внимание при распределении нагрузки.

Еще одним недостатком алгоритма является то, что он может привести к неравномерному распределению нагрузки в зависимости от того, какие ресурсы на каких серверах находятся под нагрузкой. Например, если на одном сервере находится максимальная нагрузка на процессор, тогда он может быть проигнорирован алгоритмом в пользу других серверов. Однако, если этот сервер находится на линии передачи данных, то это может привести к повышенной задержке в работе приложения или сервиса.

Несмотря на эти недостатки, алгоритм балансировки нагрузки на основе ресурсов остается одним из самых распространенных и эффективных методов балансировки нагрузки, которые используются в данных целях.

10. **Балансировка на основе географического положения** предполагает распределение запросов к серверу между несколькими серверами на разных географических местах. Это позволяет уменьшить время отклика и увеличить доступность сайта для пользователей из разных регионов. Однако, это также может привести к трудностям в синхронизации данных между серверами и нагрузке на локальные сети и соединения между серверами. Кроме того, если использовать несколько серверов, требуется управление ими и их координация, что может быть достаточно сложным.
