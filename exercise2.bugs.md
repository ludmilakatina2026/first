## Bug reports

***Баг №1***   
***Summary***    
Неправильный статус код при удалении несуществующего в системе  автора при использовании метода DELETE   
***Pre conditions***    
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Steps***  
1)выполнить запрос DELETE (с указанием в качестве значения id значение идентификатора не существующего в системе автора) для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Authors/606>     
***Post conditions***  закрыть сайт      
***Expected result***   
в результате выполнения запроса система возвращает статус код 404 not found  
***Actual result***   
в результате выполнения запроса система возвращает статус код 200 ОК  
***Severity***  minor  
***Preority***  low  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)

***Баг №2***  
***Summary***    
Отсутствуют изменения при использовании метода PUT  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Steps***  
1)выполнить PUT запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities/12>   
В теле запроса указать  
"id": 12,
"title": "hello",
"dueDate": "2023-05-16T11:58:00.654Z",
"completed": true  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities/12>  
со значением id  заданным при PUT запросе  
***Post conditions***  закрыть сайт    
***Expected result***  
В теле ответа запроса содержится информация об измененной активности "id": 12,
"title": "hello",
"dueDate": "2023-05-16T11:58:00.654Z",
"completed": true      
***Actual result***   
В теле ответа запроса содержится информация об активности без созданных изменений   
***Severity***  Critical     
***Preority***  High  
***Type***  functional      
***State***  open   
***Date of creation***  20.05.2023   
***Enviroment***  windows 10, Yandex browser Версия
23.3.3.721 (64-bit)

***Баг №3***  
***Summary***   
Неправильный статус код при создании новой активности методом POST  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить POST запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities>  
в теле запроса указать:  
"id": 1,
"title": "number1",
"dueDate": "2023-05-16T12:20:52.140Z",
"completed": true  
2)выполнить  GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities/1>
для активности со значением id ,заданным в POST запросе - 1    
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения  POST запроса система возвращает статус код 201 created
В теле ответа возвращена информация о создании новой активности 
{
  "id": 1,
  "title": "number1",
  "dueDate": "2023-05-17T13:57:33.284Z",
  "completed": true
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК 
В теле ответа  GET запроса возвращена информация о созданной активности со значениями, указанными в POST запросе 
 {
  "id": 1,
  "title": "number1",
  "dueDate": "2023-05-17T13:57:33.284Z",
  "completed": true
}  
***Actual result***  
-В результате выполнения POST запроса система возвращает статус код 200 OK. В теле ответа  возвращена информация о создании новой активности  
{
  "id": 1,
  "title": "number1",
  "dueDate": "2023-05-17T13:57:33.284Z",
  "completed": true
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК 
В теле ответа  GET запроса возвращена информация не соответствующаа информации заданной в POST запросе
{
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-05-17T14:59:15.2067175+00:00",
  "completed": false
}  
***Severity***  Critical  
***Preority***  High  
***Type***  functional    
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, Microsoft Edge
Версия 113.0.1774.42 (Официальная сборка) (64-разрядная версия)  

***Баг №4***  
***Summary***   
После удаления активности методом DELETE она остается в системе   
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить DELETE запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities/12> (указать идентификатор 12)  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Activities/12>
(указать идентификатор используемый в при запросе метода DELETE - 12)  
***Post conditions***  закрыть сайт  
***Expected result***  
В результате выполнения GET  запроса система возвращает статус код 404 not found. 
В теле ответа запроса отсутствует информация об активности с заданным идентификатором  
***Actual result***    
В результате выполнения GET запроса система возвращает статус код 200 ОК с содержащими в теле ответа объект JSON со значениями id соответствующими значению при выполнении метода DELETE  
***Severity***  Critical  
***Preority***  High  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)



***Баг №5***  
***Summary***    
Отсутствуют изменения внесенные в данные о книге при использовании метода PUT  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>    
***Steps***  
1)Выпонить PUT запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Books/25>
В теле запроса указать 
{
  "id": 25,
  "title": "alise",
  "description": "alise and her cat have a big adventure",
  "pageCount": 250,
  "excerpt": "string",
  "publishDate": "2023-05-17T05:01:26.628Z"
}  
2) Выпонить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Books/25> со значением id  заданным при PUT запросе  
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация для создания новой книги  
{
  "id": 25,
  "title": "alise",
  "description": "alise and her cat have a big adventure",
  "pageCount": 250,
  "excerpt": "string",
  "publishDate": "2023-05-17T05:01:26.628Z"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация 
{
  "id": 25,
  "title": "alise",
  "description": "alise and her cat have a big adventure",
  "pageCount": 250,
  "excerpt": "string",
  "publishDate": "2023-05-17T05:01:26.628Z"
}  
***Actual result***  
-В результате выполнения запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация для создания новой книги  
{
  "id": 25,
  "title": "alise",
  "description": "alise and her cat have a big adventure",
  "pageCount": 250,
  "excerpt": "string",
  "publishDate": "2023-05-17T05:01:26.628Z"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается преждняя  информация 
{
  "id": 25,
  "title": "Book 25",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 2500,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem ... lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-04-22T05:12:11.1047762+00:00"
}  
***Severity***  Critical  
***Preority*** Hhigh  
***Type***  functional  
  ***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, Microsoft Edge
Версия 113.0.1774.42 (Официальная сборка) (64-разрядная версия)




***Баг №6***  
***Summary***   
Неправильный статус код при создании  фото обложки  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить POST запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos> 
в теле запроса указать   
{
  "id": 15,
  "idBook": 12,
  "url": "string"
}  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/15> со значением id указанным в запросе POST  - 15   
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения  POST запроса система возвращает статус код 201 created 
В теле ответа возвращена информация для создания фото обложки
{
  "id": 15,
  "idBook": 12,
  "url": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация 
{
  "id": 15,
  "idBook": 12,
  "url": "string"
}  
***Actual result***    
-В результате выполнения POST запроса система возвращает статус код 200 OK 
В теле ответа возвращена информация для создания фото обложки
{
  "id": 15,
  "idBook": 12,
  "url": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация, не соответсвующая данным заданным в POST запросе.
{
  "id": 15,
  "idBook": 15,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 15&w=250&h=350"
}  
***Severity***  Critical  
***Preority***  High   
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)


***Баг №7***  
***Summary***    
Неправильный статус код при получении информации о фото обложки по id книги  с ошибочным id при использовании метода GET  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Steps***  
1)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/0000000222222> ( с указанием  ошибочного идентификатора книги 0000000222222)                                                                                                
***Post conditions***  закрыть сайт   
***Expected result***    
-В результате выполнения запроса система возвращает статус код 400 bad request.  
В теле ответа JSON  Схема  отображена корректно: имена и типы полей соответствует ожидаемым , включая вложенные объекты. Значения полей соответствует значениям тестовых данных. Где id является целым числом.
title - строка, допускающая пустое значение
status отображает соответствующее значение ошибки

***Actual result***    
-В результате выполнения запроса система возвращает статус код 200 ОК 
В теле ответа отображается - []  
***Severity***  Critical  
***Preority***  High  
***Type*** functional   
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)


***Баг №8***  
***Summary***    
Отсутствуют изменения внесенные в информацию о фото обложки при использовании метода PUT  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>    
***Steps***  
1)Выпонить PUT запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/120>
В теле запроса указать 
{
  "id": 120,
  "idBook": 150,
  "url": "string"
}  
2) Выпонить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/120> со значением id  заданным при PUT запросе    
***Post conditions***  закрыть сайт    
***Expected result***   
-В результате выполнения запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация оь изменении фото обложки
{
  "id": 120,
  "idBook": 150,
  "url": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация 
{
  "id": 120,
  "idBook": 150,
  "url": "string"
}  
***Actual result***    
-В результате выполнения запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация об изменении фото обложки
{
  "id": 120,
  "idBook": 150,
  "url": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается преждняя  информация 
{
  "id": 120,
  "idBook": 120,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 120&w=250&h=350"
}  
***Severity***  Critical  
***Preority*** Hhigh  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит) 

***Баг №9***  
***Summary***    
После удаления фото обложки методом DELETE она остается в системе   
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>
***Step***    
1)выполнить DELETE запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/120> (указать идентификатор 120)    
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/120> 
(указать идентификатор используемый в при запросе метода DELETE - 120)                                                                                                 
***Post conditions***  закрыть сайт     
***Expected result***     
В результате выполнения GET  запроса система возвращает статус код 404 not found. 
В теле ответа запроса отсутствует информация о фото обложки с заданным идентификатором  
***Actual result***   
В результате выполнения GET запроса система возвращает статус код 200 ОК с содержащими в теле ответа объект JSON со значениями id соответствующими значению при выполнении метода DELETE  
{
  "id": 120,
  "idBook": 120,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 120&w=250&h=350"
}  
***Severity***  Critical  
***Preority***  High  
***Type*** functional   
***Assigned***  разработчик ФИО  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)


***Баг №10***   
***Summary***    
Неправильный статус код при удалении несуществующей в системе  фото обложки при использовании метода DELETE    
***Pre conditions***    
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Steps***  
1)выполнить DELETE запрос  (с указанием в качестве значения id значение идентификатора не существующей в системе фото обложки) для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/22222>      
***Post conditions***  закрыть сайт      
***Expected result***   
В результате выполнения запроса система возвращает статус код 404 not found
***Actual result***    
В результате выполнения запроса система возвращает статус код 200 ОК    
***Severity***  minor  
***Preority***  low  
***Type***  functional  
 ***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)

***Баг №11***  
***Summary***   
Неправильный статус код при создании нового пользователя  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить POST запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Users> 
в теле запроса указать   
{
  "id": 12,
  "userName": "string",
  "password": "string"
}   
2)выполнить  GET запрос для эндпоинта  <https://fakerestapi.azurewebsites.net/api/v1/Users/12> для пользователя со значением id ,заданным в POST запросе - 12   
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения POST запроса система возвращает статус код 201 created 
В теле ответа возвращена информация о создании нового пользователя {
  "id": 12,
  "userName": "string",
  "password": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК 
В теле ответа  GET запроса возвращена информация о созданном пользователе со значениями, указанными в POST запросе 
 {
  "id": 12,
  "userName": "string",
  "password": "string"
}  
***Actual result***    
-В результате выполнения POST запроса система возвращает статус код 200 OK. В теле ответа  возвращена информация о создании нового пользователя {
  "id": 12,
  "userName": "string",
  "password": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 404 Error: Not Found
В теле ответа  GET запроса возвращена информация об ошибке {
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-ae76523c22755f4eb48325d6e2206d30-28e22eab8167e74c-00"
}  
***Severity***  Critical  
***Preority***  High  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, windows 10, Yandex browser Версия
23.3.3.721 (64-bit)

***Баг №12***  
***Summary***    
Отсутствуют изменения внесенные в информацию о пользователе при использовании метода PUT  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>    
***Steps***  
1)Выпонить PUT запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Users/5>
В теле запроса указать 
{
  "id": 5,
  "userName": "funfun",
  "password": "string"
}  
2) Выпонить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Users/5> со значением id  заданным при PUT запросе  
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения PUT  запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация об изменении информации о пользователе
{
  "id": 5,
  "userName": "funfun",
  "password": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация о внесенных изменениях  
{
  "id": 5,
  "userName": "funfun",
  "password": "string"
}  
***Actual result***    
-В результате выполнения PUT запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация об изменении информации о пользователе
{
  "id": 5,
  "userName": "funfun",
  "password": "string"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается преждняя  информация 
{
  "id": 5,
  "userName": "User 5",
  "password": "Password5"
}   
***Severity***  Critical  
***Preority*** Hhigh  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит) 


***Баг №13***  
***Summary***   
Неправильный статус код при добавлении нового автора  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить POST запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Authors> 
в теле запроса указать   
{
  "id": 12,
  "idBook": 13,
  "firstName": "Jack",
  "lastName": "London"
}  
2)выполнить  GET запрос для эндпоинта для эндпоинта  <https://fakerestapi.azurewebsites.net/api/v1/Authors/12> для автора со значением id ,заданным в POST запросе - 12   
***Post conditions***  закрыть сайт  
***Expected result***    
-В результате выполнения POST запроса система возвращает статус код 201 created 
В теле ответа возвращена информация о создании нового автора 
{
  "id": 12,
  "idBook": 13,
  "firstName": "Jack",
  "lastName": "London"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК 
В теле ответа  GET запроса возвращена информация о созданном авторе со значениями, указанными в POST запросе 
 {
  "id": 12,
  "idBook": 13,
  "firstName": "Jack",
  "lastName": "London"
}  
***Actual result***    
-В результате выполнения POST запроса система возвращает статус код 200 OK. В теле ответа  возвращена информация о создании нового автора {
  "id": 12,
  "idBook": 13,
  "firstName": "Jack",
  "lastName": "London"
}  
-В результате выполнения GET запроса система возвращает  статус код 200 OK
В теле ответа  GET запроса возвращена информация с данными не соответсвующими данным отправленным в POST запросе {
  "id": 12,
  "idBook": 4,
  "firstName": "First Name 12",
  "lastName": "Last Name 12"
}  
***Severity***  Critical  
***Preority***  High  
***Type***  functional  
 ***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)


***Баг №14***  
***Summary***    
После удаления пользователя методом DELETE он остается в системе   
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Step***  
1)выполнить DELETE запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Users/4>(указать идентификатор 4)  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Users/4>
(указать идентификатор используемый в при запросе метода DELETE - 4)                                                                                               
***Post conditions***  закрыть сайт   
***Expected result***     
В результате выполнения GET  запроса система возвращает статус код 404 not found. 
В теле ответа GET запроса отсутствует информация о пользователе  с заданным идентификатором  
***Actual result***    
В результате выполнения GET запроса система возвращает статус код 200 ОК с содержащими в теле ответа объект JSON со значениями id, соответствующими значению при выполнении метода DELETE  
{
  "id": 4,
  "userName": "User 4",
  "password": "Password4"
}  
***Severity***  Critical  
***Preority***  High  
***Type*** functional   
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)

***Баг №15***  
***Summary***    
Отсутствуют изменения внесенные в информацию об авторе при использовании метода PUT  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>    
***Steps***  
1)Выпонить PUT запрос <https://fakerestapi.azurewebsites.net/api/v1/Authors/12>
В теле запроса указать 
{
  "id": 12,
  "idBook": 35,
  "firstName": "Sam",
  "lastName": "Crudge"
}  
2) Выпонить GET запрос <https://fakerestapi.azurewebsites.net/api/v1/Authors/12> со значением id  заданным при PUT запросе - 12  
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения PUT  запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация об изменении информации об авторе
{
  "id": 12,
  "idBook": 35,
  "firstName": "Sam",
  "lastName": "Crudge"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается информация о внесенных изменениях  
{
  "id": 12,
  "idBook": 35,
  "firstName": "Sam",
  "lastName": "Crudge"
}  
***Actual result***    
-В результате выполнения PUT запроса система возвращает статус код 200 ОК.  
В теле ответа возвращена информация об изменении информации об авторе
{
  "id": 12,
  "idBook": 35,
  "firstName": "Sam",
  "lastName": "Crudge"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК.  
В теле ответа GET запроса отображается преждняя  информация 
{
  "id": 12,
  "idBook": 4,
  "firstName": "First Name 12",
  "lastName": "Last Name 12"
}  
***Severity***  Critical  
***Preority*** Hhigh  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит) 

***Баг №16***  
***Summary***    
После удаления автора методом DELETE он остается в системе   
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>
***Steps***  
1)выполнить DELETE запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Authors/12>(указать идентификатор 12)  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Authors/12>
(указать идентификатор используемый в при запросе метода DELETE - 12)                                                                                               
***Post conditions***  закрыть сайт   
***Expected result***     
В результате выполнения GET  запроса система возвращает статус код 404 not found.   
В теле ответа GET запроса отсутствует информация о пользователе  с заданным идентификатором  
***Actual result***    
В результате выполнения GET запроса система возвращает статус код 200 ОК с содержащими в теле ответа объект JSON со значениями id, соответствующими значению при выполнении метода DELETE  
{
  "id": 12,
  "idBook": 4,
  "firstName": "First Name 12",
  "lastName": "Last Name 12"
}  
***Severity***  Critical  
***Preority***  High  
***Type*** functional   
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)

***Баг №17***  
***Summary***   
Неправильный статус код при добавлении новой книги  
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>   
***Steps***  
1)выполнить POST запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Books>
в теле запроса указать   
{
  "id": 212,
  "title": "rose",
  "description": "girl rose in wonderland",
  "pageCount": 150,
  "excerpt": "string",
  "publishDate": "2023-05-17T13:42:25.583Z"
}  
2)выполнить  GET запрос для эндпоинта  <https://fakerestapi.azurewebsites.net/api/v1/Books/212> для книги со значением id ,заданным в POST запросе - 212   
***Post conditions***  закрыть сайт  
***Expected result***   
-В результате выполнения POST запроса система возвращает статус код 201 created 
В теле ответа возвращена информация о создании новой книги 
{
  "id": 212,
  "title": "rose",
  "description": "girl rose in wonderland",
  "pageCount": 150,
  "excerpt": "string",
  "publishDate": "2023-05-17T13:42:25.583Z"
}  
-В результате выполнения GET запроса система возвращает статус код 200 ОК 
В теле ответа  GET запроса возвращена информация о созданной книге со значениями, указанными в POST запросе 
 {
  "id": 212,
  "title": "rose",
  "description": "girl rose in wonderland",
  "pageCount": 150,
  "excerpt": "string",
  "publishDate": "2023-05-17T13:42:25.583Z"
}  
***Actual result***    
-В результате выполнения POST запроса система возвращает статус код 200 OK. В теле ответа  возвращена информация о создании новой книги  
{
  "id": 212,
  "title": "rose",
  "description": "girl rose in wonderland",
  "pageCount": 150,
  "excerpt": "string",
  "publishDate": "2023-05-17T13:42:25.583Z"
}  
-В результате выполнения GET запроса система возвращает статус код 404 Error: Not Found
В теле ответа  GET запроса возвращена информация об ошибке 
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-e4dd3436742e3b4d891eef3596d2fac2-99a4d3d1c7b8694b-00"
}   
***Severity***  Critical  
***Preority***  High  
***Type***  functional  
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, google chrome Версия 112.0.5615.49 (Официальная сборка), (64 бит)


***Баг №18***  
***Summary***   
После удаления книги методом DELETE она остается в системе   
***Pre conditions***   
Открыта  страница сайта <https://fakerestapi.azurewebsites.net/index.html>  
***Steps***    
1)выполнить DELETE запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Books/25>(указать идентификатор 25)  
2)выполнить GET запрос для эндпоинта <https://fakerestapi.azurewebsites.net/api/v1/Books/25>
(указать идентификатор используемый в при запросе метода DELETE - 25)                                                                                               
***Post conditions***  закрыть сайт   
***Expected result***     
В результате выполнения GET  запроса система возвращает статус код 404 not found. 
В теле ответа GET запроса отсутствует информация о пользователе  с заданным идентификатором
***Actual result***    
В результате выполнения GET запроса система возвращает статус код 200 ОК с содержащими в теле ответа объект JSON со значениями id, соответствующими значению при выполнении метода DELETE  
{
  "id": 25,
  "title": "Book 25",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 2500,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem ... lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-04-22T13:51:57.8640972+00:00"
}  
***Severity***  Critical  
***Preority***  High  
***Type*** functional     
***State***  open  
***Date of creation***  20.05.2023  
***Enviroment***  windows 10, Microsoft Edge
Версия 113.0.1774.42 (Официальная сборка) (64-разрядная версия)