# XML, HTML и XPath

*Что такое языки разметки?*

**Язык разметки** - это формальный язык, который используется для описания структуры и содержимого документов. Язык разметки обычно состоит из набора тегов (markup tags), которые позволяют определить, как следует отображать текст и другие элементы документа.

*Какие вы знаете языки разметки? Опишите каждый из них.*

**XML (Extensible Markup Language)** - это универсальный язык разметки, который используется для хранения и передачи данных. XML был разработан с целью обеспечения универсальности и расширяемости, что позволяет использовать его в различных областях, от научных исследований до веб-приложений.

**HTML (Hypertext Markup Language)** - это язык разметки, который используется для создания веб-страниц. HTML определяет, как следует отображать текст, изображения, ссылки и другие элементы веб-страницы.

**SGML (Standard Generalized Markup Language)** - это язык разметки, который был разработан в 1980-х годах для описания документов в различных областях, от научных исследований до авиационной промышленности. SGML является предшественником XML.

**XHTML (Extensible Hypertext Markup Language)** - это язык разметки, который является комбинацией XML и HTML. XHTML следует синтаксису XML, что позволяет использовать его в XML-приложениях, но он также сохраняет совместимость с HTML.

**Markdown** - это простой язык разметки, который используется для форматирования текста. Markdown обычно используется для написания документации, блогов и других текстовых материалов, которые требуют минимальной разметки.

*Что такое теги, атрибуты и элементы в XML?*

В **XML (Extensible Markup Language)** теги, атрибуты и элементы используются для определения структуры и содержимого документа. Вот более подробное описание каждого из них:

***Теги*** - это ключевые слова, которые используются для определения типа элемента в документе. В XML теги используются для описания начала и конца элемента. Каждый элемент должен иметь открывающий и закрывающий тег, которые обрамляют его содержимое. Например, **`<book>`** - это открывающий тег, а **`</book>`** - закрывающий тег элемента, который содержит информацию о книге. Если элемент не имеет содержимого, то можно использовать одиночный тег. Одиночный тег представляет собой открывающий тег, за которым не следует закрывающий тег. Например, **`<author name="Jane Austen"/>`** - это одиночный тег, который содержит атрибут "name" со значением *"Jane Austen"*.

***Атрибуты*** - это дополнительная информация, которая может быть связана с элементом в документе. Атрибуты представляют собой пары имя/значение, которые указываются в открывающем теге элемента. Например, в элементе **`<book id="123">`** атрибут *"id"* имеет значение *"123"*.

***Элементы*** - это основные строительные блоки документа в XML. Элементы содержат информацию о данных, которые вы хотите хранить или передавать. Элементы могут содержать текст, другие элементы, атрибуты и комментарии. Например, элемент **`<title>`** содержит заголовок книги.

*Чем XML отличается от HTML?*

**XML и HTML (Hypertext Markup Language)** - это два разных языка разметки, которые используются для разных целей.

Основное отличие между XML и HTML заключается в том, что HTML разработан для отображения информации в браузере, в то время как XML предназначен для хранения и передачи данных.

HTML имеет ограниченный набор тегов и правил для форматирования документов, в основном используется для создания веб-страниц. В HTML существует заранее определенный набор тегов, который определяет содержимое и отображение документа. Кроме того, HTML позволяет вставлять скрипты, которые позволяют создавать динамические веб-страницы.

XML, с другой стороны, предназначен для хранения и обмена данными между различными приложениями и системами. В XML не существует заранее определенного набора тегов, а разработчики могут определить свои собственные теги в соответствии с требованиями. В XML можно использовать атрибуты, чтобы добавлять дополнительную информацию о данных.

Еще одно отличие между XML и HTML состоит в том, что XML является более строгим языком разметки, чем HTML. XML требует, чтобы каждый открывающий тег имел соответствующий закрывающий тег, а каждый элемент должен иметь корневой элемент.

В целом, XML и HTML предназначены для разных целей и имеют различные особенности. HTML используется для создания веб-страниц и отображения информации в браузере, а XML используется для хранения и передачи данных между приложениями и системами.

*Что такое DTD? Расшифруйте и приведите примеры использования в различных документах.*

**DTD (Document Type Definition)** - это набор правил, который определяет структуру и формат XML-документа. DTD описывает, какие элементы и атрибуты могут использоваться в документе, а также какие значения могут принимать эти элементы и атрибуты. DTD может быть включен в сам XML-документ в виде отдельного блока, который определяет структуру документа. DTD можно также хранить в отдельном файле и подключать его к XML-документу.

*Книжный каталог*

: DTD может использоваться для описания структуры и формата книжного каталога, например:

![](https://downloader.disk.yandex.ru/preview/30aff392c780e79bbaceead1440eafba1f0524c8e242f5e60f999e84c467059a/645268f0/TvOJsoVMewxS0-tgB3CcABLPOdkXSiULt2ytLAO0xEmA2oazGoP2rO3yt2Wrw0GSVZnN5nAC_MIn7dJjjwMcGg%3D%3D?uid=0&filename=1.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

*Календарь*

: DTD может использоваться для описания структуры и формата календарного документа, например:

![](https://downloader.disk.yandex.ru/preview/94daab84b153e42acbfa1cac098a9973c4b770b05e00466a6a2d58fe72515a57/64526953/zrUWYAgUEtqGxOp6abk8sRLPOdkXSiULt2ytLAO0xEmB1EV1U-XmRZh3D97Kga34BllBZcPB4RUr9JaFuPuNrw%3D%3D?uid=0&filename=2.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

*Веб-страница*

: DTD может использоваться для описания структуры и формата веб-страницы, например:

![](https://downloader.disk.yandex.ru/preview/b517000b8b3a90087b93156c98e0089401e629228ccfef227f248c3fdee9c339/6452697f/iTNkI8EULZ8fmwYpDw0y3xDc51bsSKxsCEM__gh_icIVPLep4WNTCRZVHlqd_IMTNduLoCyc6WMC0MztcV-2VQ%3D%3D?uid=0&filename=3.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

*Электронная почта*: DTD может использоваться для описания структуры и формата электронного письма, например:

![](https://downloader.disk.yandex.ru/preview/e372608f9e50ae23d31cd48772b0d9f12e4ba843388b0f0277d1d18bd8e7e9a3/64526a5c/tPCy8yccoG483-Zom8mAyxLPOdkXSiULt2ytLAO0xEllg4cbBQRZvNh8dg1QWLkYZBlcR-DOuvZzDLLOIJMUwg%3D%3D?uid=0&filename=4.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

*Из чего состоит HTML-документ?*

**HTML-документ** состоит из нескольких составных частей, которые определяют его структуру и содержание. Вот более подробная информация об этих частях:

- ***Doctype***: это объявление типа документа, которое определяет версию HTML, которую следует использовать. Оно должно быть расположено в самом начале HTML-документа и имеет следующий синтаксис:

![](https://downloader.disk.yandex.ru/preview/bd177a7307d7c995fcad9112b0a6866454b85e66f3764871c7a2bd3083b94ebe/64526a9c/GZwlI8OPXbqXEaMOw_rt9p5n2ASALjVaHUkyYmEhEqwtFKnatFIfQ8yfwjrH19zHBkVakKUveAwEMDRkgj16gw%3D%3D?uid=0&filename=5.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

Эта строка указывает на то, что документ следует интерпретировать как *HTML5*.

- ***Корневой тег html***: HTML-документ начинается с тега html, который определяет начало и конец HTML-кода. Этот тег имеет два обязательных атрибута: *lang* (определяет язык документа) и *dir* (определяет направление текста).

![](https://downloader.disk.yandex.ru/preview/2ce2b4e1b30081e151a2e4a70a354c0c0d3e4619b0efb824f1967eb09a02f1cb/64526aed/HQ0sKDX56F-1Iltw7m8pWxLPOdkXSiULt2ytLAO0xEn8DREweB-a8A25e_wyOskLFtDiRQvuM8wvOfcJgpHSwQ%3D%3D?uid=0&filename=6.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

- ***DOM (Document Object Model)*** - это древовидная структура, представляющая HTML-документ как объект. Каждый элемент HTML-документа является узлом дерева DOM, а атрибуты и содержимое элементов являются свойствами и методами этих узлов.
- ***Теги head***: элемент head содержит метаданные документа, такие как заголовок страницы, мета-теги, скрипты и стили. Например:

![](https://downloader.disk.yandex.ru/preview/2ce2b4e1b30081e151a2e4a70a354c0c0d3e4619b0efb824f1967eb09a02f1cb/64526aed/HQ0sKDX56F-1Iltw7m8pWxLPOdkXSiULt2ytLAO0xEn8DREweB-a8A25e_wyOskLFtDiRQvuM8wvOfcJgpHSwQ%3D%3D?uid=0&filename=6.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048)

Таким образом, HTML-документ состоит из объявления типа документа, корневого тега html, древовидной структуры DOM, элементов head и body, которые содержат метаданные и основное содержимое документа соответственно.
