# exercise3.2

Кнопки:

1. Квартира:

> //span[text()[contains(.,'Квартира')]]/parent::button
> 
1. Дом

> //span[text()[contains(.,'Дом')]]/parent::button
> 
1. Сдается в аренду - Да

> //*[text()[contains(.,'Сдается в аренду')]]/..//*[text()[contains(.,'Да')]]/..
> 
1. Сдается в аренду - Нет

> //*[text()[contains(.,'Сдается в аренду')]]/..//*[text()[contains(.,'Нет')]]/..
> 

Для ветки “Квартира”: 

1. Расположена на первом или последнем этаже - Да 

//*[text()[contains(.,'Расположена на первом или последнем этаже
')]]/..//*[text()[contains(.,'Да')]]/..

1. Расположена на первом или последнем этаже - Нет

> //*[text()[contains(.,'Расположена на первом или последнем этаже
')]]/..//*[text()[contains(.,'Нет')]]/..
> 
1. Установлена охранная сигнализация - Да

> //*[text()[contains(.,'Установлена охранная сигнализация
')]]/..//*[text()[contains(.,'Да')]]/..
> 
1. Установлена охранная сигнализация - Нет

> //*[text()[contains(.,'Установлена охранная сигнализация
')]]/..//*[text()[contains(.,'Нет')]]/..
> 

Для ветки “Дом”: 

1. Установлена охранная сигнализация - Да

> //*[text()[contains(.,'Установлена охранная сигнализация
')]]/..//*[text()[contains(.,'Да')]]/..
> 
1. Установлена охранная сигнализация - Нет

> //*[text()[contains(.,'Установлена охранная сигнализация
')]]/..//*[text()[contains(.,'Нет')]]/..
> 
1. Материал несущих стен - Кирпич или монолит

> //*[text()[contains(.,'Материал несущих стен')]]/..//*[text()[contains(.,'Кирпич или монолит')]]/..
> 
1. Материал несущих стен - Дерево

> //*[text()[contains(.,'Материал несущих стен')]]/..//*[text()[contains(.,'Дерево')]]/..
> 
1. Применить 

> //div[@class='reg-button']//button[@class='mat-focus-indicator action-back mat-stroked-button mat-button-base']
> 
1. Оформить

> //div[@class='form-actions']//button[@class='mat-focus-indicator mat-stroked-button mat-button-base mat-accent']
> 
1. Заполнить по Сбер ID

> //div[@class='contact-form__insurant-item ng-star-inserted']//button[@class='mat-focus-indicator sber-id-button mat-flat-button mat-button-base mat-primary']
> 
1. Мужской  

> //span[text()='Мужской']/ancestor::button
> 
1. Женский

> //span[text()='Женский']/ancestor::button
> 
1. Вернуться

> //div[@class='form-actions']//button[@class='mat-focus-indicator action-back mat-stroked-button mat-button-base']
> 
1. Оформить

> //div[@class='form-actions']//button[@class='mat-focus-indicator mat-flat-button mat-button-base mat-accent']
> 

Поля ввода:

1. Промокод   //input[@formcontrolname="promoCode"]
2. Фамилия  //input[@formcontrolname="lastName" and @data-placeholder="Фамилия"]
3. Имя  //input[@formcontrolname="name" and @data-placeholder="Имя"]
4. Отчество  //input[@formcontrolname="middleName" and @data-placeholder="Отчество"]
5. Серия  //input[@formcontrolname="docSeries"and @data-placeholder="Серия"]
6. Номер //input[@formcontrolname="docNumber" and @data-placeholder="Номер"]
7. Кем выдан  //textarea[@formcontrolname="docIssuer"]
8. Код подразделения  //input[@formcontrolname="docDepartmentCode" and @data-placeholder="Код подразделения"]
9. Город или населенный пункт  //input[@formcontrolname="registrationCity" and @data-placeholder="Город или населенный пункт"]
10. Улица  //input[@formcontrolname="registrationStreet" and @data-placeholder="Улица"]
11. Дом, литера, корпус, строение  //input[@formcontrolname="registrationHouse" and @data-placeholder="Дом, литера, корпус, строение"]
12. Квартира //input[@formcontrolname="registrationFlat" and @data-placeholder="Квартира"]
13. Телефон  //input[@formcontrolname="contactPhone" and @data-placeholder="Телефон"]
14. Электронная почта  //input[@formcontrolname="contactEmail" and @data-placeholder="Электронная почта *"]
15. Повтор электронной почты   //input[@formcontrolname="repeatEmail" and @data-placeholder="Повтор электронной почты *"]

Чек-боксы:

1. Отчество отсутствует   //div[@class='contact-form__control checkbox']//input[@type="checkbox"]
2. Улица отсутствует   //div[@class='contact-form__control street-checkbox']//input[@type="checkbox"]

Датапикеры:  

1. Срок действия страхования - дата начала    //input[@formcontrolname="startDate"]
2. Дата рождения  //input[@formcontrolname="birthDate"]   
3. Дата выдачи   //input[@formcontrolname="docDate"]   

Логотип "СБЕР СТРАХОВАНИЕ”     //div[@class='header__content']//div[@class='sber-logo']

Хедер "Что будет застраховано?"      //h4[contains(text(), "Что будет застраховано?")]

Текстовый блок "Мебель, техника и ваши вещи":

//div[text()[contains(.,'Мебель, техника и ваши вещи')]]

Текстовый блок "Падение летательных аппаратов и их частей":

//div[text()[contains(.,'Падение летательных аппаратов и их частей')]]

Левая колонка в списке, который находится под хедером "Страховая защита включенная в программу":

//div[ul[@class='calc-description__list']][1]

Правая колонка в списке, который находится под хедером "Страховая защита включенная в программу":

//div[ul[@class='calc-description__list']][2]